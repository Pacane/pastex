/* -*- Mode: Javascript; indent-tabs-mode:nil; js-indent-level: 2 -*- */
/* vim: set ts=2 et sw=2 tw=80: */

/*************************************************************
 *
 *  MathJax/config/local/local.js
 *
 *  Include changes and configuration local to your installation
 *  in this file.  For example, common macros can be defined here
 *  (see below).  To use this file, add "local/local.js" to the
 *  config array in MathJax.js or your MathJax.Hub.Config() call.
 *
 *  ---------------------------------------------------------------------
 *
 *  Copyright (c) 2009-2013 The MathJax Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


MathJax.Hub.Register.StartupHook("TeX Jax Ready", function () {
	var TEX = MathJax.InputJax.TeX;

	// Valeurs booléennes
	TEX.Macro("vrai", "\\textbf{vrai}");
	TEX.Macro("faux", "\\textbf{faux}");

	// Opérateurs booléens
	TEX.Macro("non", "\\not");
	TEX.Macro("et", "\\wedge");
	TEX.Macro("ou", "\\vee");
	TEX.Macro("implique", "\\Rightarrow");
	TEX.Macro("impliqueinv", "\\LeftArrow");
	TEX.Macro("ssi", "\\Leftrightarrow");

	// Opérateurs ensemblistes
	TEX.Macro("dans", "\\in");
	TEX.Macro("inclus", "\\subseteq");
	TEX.Macro("strictinclus", "\\subset");
	TEX.Macro("inclusinv", "\\supseteq");
	TEX.Macro("strictinclusinv", "\\supset");

	TEX.Macro("nondans", "\\notin");
	TEX.Macro("noninclus", "\\not\\subseteq");
	TEX.Macro("nonstrictinclus", "\\not\\subset");

	TEX.Macro("inter","\\cap");
	TEX.Macro("union","\\cup");
	TEX.Macro("comp","\\mbox{\\footnotesize $c$}");
	TEX.Macro("moins","\\setminus");

	// Ensembles fréquemments utilisés %%%
	TEX.Macro("ensembleVide", "\\emptyset");
	TEX.Macro("ensembleU","\\mathbf{U}");
	TEX.Macro("ensembleB","\\mathbb{B}");
	TEX.Macro("ensembleN","\\mathbb{N}");
	TEX.Macro("ensembleZ","\\mathbb{Z}");
	TEX.Macro("ensembleR","\\mathbb{R}");
	TEX.Macro("ensembleQ","\\mathbb{Q}");
	TEX.Macro("ensembleI","\\mathbb{I}");

	// Relations  %%%
	TEX.Macro("compose", "\\circ");
	TEX.Macro("croix", "\\times");
	TEX.Macro("Identite", "\\mathbf{I}");
	TEX.Macro("Domaine", "\\mbox{\\rm Dom}");
	TEX.Macro("Image","\\mbox{\\rm Im}");
	//TEX.Macro("tuple[1]","\\ensuremath{\\left\\langle #1 \\right\\rangle}"); //TODO: Corriger celui-là


	// place macros here.  E.g.:
	//   TEX.Macro("R","{\\bf R}");
	//   TEX.Macro("op","\\mathop{\\rm #1}",1); // a macro with 1 parameter

});

MathJax.Ajax.loadComplete("[MathJax]/config/local/local.js");
