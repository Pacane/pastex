$(function () {


    CodeMirror.fromTextArea(document.getElementById("codeBox"),
        {
            indentWithTabs: true,
            lineNumbers: true,
            showCursorWhenSelecting: true
        }
    );


    $("#generateButton").click(function () {
        var latexCode = $('textarea#codeBox').val();
        $('#resultBox').empty().append("\\(").append(latexCode).append("\\)");
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
    });
});




